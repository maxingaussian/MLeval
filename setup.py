from setuptools import setup, find_packages

setup(
    name='MLeval',
    version='1.0.0',
    url='https://gitlab.com/MaxInGaussian/MLeval',
    packages=find_packages(),
    author='Max W. Y. Lam',
    author_email="maxingaussian@gmail.com",
    description='Standarized Evaluation Package for Machine Learning Models',
    install_requires=[
        "numpy >= 1.9.0",
        "scipy >= 0.14.0",
    ],
)