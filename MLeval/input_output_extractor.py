from __future__ import print_function
from __future__ import division
from sys import stdout
from MLeval.data_downloader import DataDownloader

class InputOutputExtractor(object):
    
    BCOLOUR = {
        'purple': '\033[95m',
        'yellow': '\033[93m',
        'blue': '\033[94m',
        'green': '\033[92m',
        'red': '\033[91m',
        'cyan': '\033[36m',
        'black': '\033[0m'
    }
    SEED = 4318622
    
    def __init__(self, n_splits, train_test_ratio=0.9, data_cache_dir='./data_cache/'):
        self.n_splits = n_splits
        self.train_test_ratio = train_test_ratio
        self.downloader = DataDownloader(data_cache_dir)
        
    def boston_dataset(self):
        data = self.downloader.read_data('boston.zip')
        return self._split_into_train_test_datasets(data, list(range(13)), [13])
        
    def concrete_dataset(self):
        data = self.downloader.read_data('concrete.zip')
        return self._split_into_train_test_datasets(data, list(range(8)), [8])
        
    def energy_dataset(self):
        data = self.downloader.read_data('energy.zip')
        return self._split_into_train_test_datasets(data, list(range(8)), [8])
        
    def kin8nm_dataset(self):
        data = self.downloader.read_data('kin8nm.zip')
        return self._split_into_train_test_datasets(data, list(range(8)), [8])
        
    def naval_dataset(self):
        data = self.downloader.read_data('naval.zip')
        return self._split_into_train_test_datasets(data, list(range(16)), [16])
        
    def power_dataset(self):
        data = self.downloader.read_data('power.zip')
        return self._split_into_train_test_datasets(data, list(range(4)), [4])
        
    def protein_dataset(self):
        data = self.downloader.read_data('protein.zip')
        return self._split_into_train_test_datasets(data, list(range(9)), [9])
        
    def wine_dataset(self):
        data = self.downloader.read_data('wine.zip')
        return self._split_into_train_test_datasets(data, list(range(11)), [11])
        
    def yacht_dataset(self):
        data = self.downloader.read_data('yacht.zip')
        return self._split_into_train_test_datasets(data, list(range(6)), [6])
        
    def _split_into_train_test_datasets(self, data, X_indices, Y_indices):
        import numpy as np
        np.random.seed(self.SEED) # FIXED random seed
        datasets = []
        print(self.BCOLOUR['purple']+'splitting data into train/test sets ......')
        stdout.flush()
        for i in range(self.n_splits):
            permutation = np.random.choice(len(data), len(data), replace=False)
            end_train = round(len(data)*self.train_test_ratio)
            train_data = data[permutation[:end_train]]
            X_train, Y_train = train_data[:, X_indices], train_data[:, Y_indices]
            test_data = data[permutation[end_train:len(data)]]
            X_test, Y_test = test_data[:, X_indices], test_data[:, Y_indices]
            datasets.append([X_train, Y_train, X_test, Y_test])
        print(self.BCOLOUR['black']+'done.')
        stdout.flush()
        return datasets
        
        