from __future__ import print_function
from __future__ import division
import time
import numpy as np
from sys import stdout
from scipy.stats import norm
from MLeval.input_output_extractor import InputOutputExtractor

class RegressorEvaluator(object):
    
    BCOLOUR = {
        'purple': '\033[95m',
        'yellow': '\033[93m',
        'blue': '\033[94m',
        'green': '\033[92m',
        'red': '\033[91m',
        'cyan': '\033[36m',
        'black': '\033[0m'
    }
    datasets = {
        'boston',
        'concrete',
        'energy',
        'kin8nm',
        'naval',
        'power',
        'protein',
        'wine',
        'yacht',
    }
    metrics = {
        'time',
        'mll',
        'mae',
        'rmse',
    }
    
    def __init__(self, n_splits, train_test_ratio=0.9, data_cache_dir='./data_cache/'):
        self.extractor = InputOutputExtractor(n_splits, train_test_ratio, data_cache_dir)
        self.char_per_line = 88
        
    def evaluate_regressor_in_dataset(self, dataset, get_regressor_func, train_func, predict_func):
        dataset = dataset.lower()
        assert dataset in self.datasets, 'dataset '+dataset+' not found!'
        train_test_splits = getattr(self.extractor, dataset+'_dataset')()
        overall_performance = {metric:[] for metric in self.metrics}
        self._print_header()
        for split_number, train_test_data in enumerate(train_test_splits):
            X_train, Y_train, X_test, Y_test = train_test_data
            split_performance = self._evaluate_regressor_in_split(
                split_number, X_train, Y_train, X_test, Y_test,
                get_regressor_func, train_func, predict_func)
            for metric in self.metrics:
                overall_performance[metric].append(split_performance[metric])
            self._print_start_of_row()
            self._print_cell('{:>5d}'.format(split_number+1), width=5)
            m, s = divmod(split_performance['time'], 60)
            self._print_cell('{:>03d}m{:>02d}s'.format(int(m), int(s)), width=7)
            self._print_cell("{0: ^{1}.8f}".format(split_performance['rmse'], 22))
            self._print_cell("{0: ^{1}.8f}".format(split_performance['mae'], 19))
            self._print_cell("{0: ^{1}.8f}".format(split_performance['mll'], 19))
            self._print_end_of_row()
        self._print_horizontal_line()
        self._print_start_of_row()
        self._print_cell('ALL', 'red', 5)
        m, s = divmod(np.mean(overall_performance['time']), 60)
        self._print_cell('{:>03d}m{:>02d}s'.format(int(m), int(s)), width=7)
        self._print_cell("{0: >{1}.3f} +/- {2: <{3}.3f}".format(
            np.mean(overall_performance['rmse']), 9, 1.96*np.std(overall_performance['rmse']), 8))
        self._print_cell("{0: >{1}.3f} +/- {2: <{3}.3f}".format(
            np.mean(overall_performance['mae']), 7, 1.96*np.std(overall_performance['mae']), 7))
        self._print_cell("{0: >{1}.3f} +/- {2: <{3}.3f}".format(
            np.mean(overall_performance['mll']), 7, 1.96*np.std(overall_performance['mll']), 7))
        self._print_end_of_row()
        self._print_horizontal_line()
        return overall_performance
        
    def _evaluate_regressor_in_split(self, split_number, X_train, Y_train, X_test, Y_test,
                                     get_regressor_func, train_func, predict_func):
        regressor = get_regressor_func()
        start_time = -time.time()
        regressor = train_func(regressor, X_train, Y_train)
        elapsed_time = start_time+time.time()
        performance = {}
        performance['time'] = elapsed_time
        Y_pred_mean, Y_pred_std = predict_func(regressor, X_test)
        performance['mll'] = np.mean(norm.logpdf(Y_test, loc=Y_pred_mean, scale=Y_pred_std))
        Y_pred_diff = np.abs(Y_test-Y_pred_mean)
        performance['mae'] = np.mean(Y_pred_diff)
        performance['rmse'] = np.mean(Y_pred_diff**2)**.5
        return performance
        
    def _print_header(self):
        self._print_cell('Evaluation Result', 'purple', self.char_per_line, '\n')
        self._print_horizontal_line()
        self._print_start_of_row()
        self._print_cell('Split', 'blue', 5)
        self._print_cell('Runtime', 'blue', 7)
        self._print_cell('Root Mean Square Error', 'blue', 22)
        self._print_cell('Mean Absolute Error', 'blue', 19)
        self._print_cell('Mean Log-Likelihood', 'blue', 19)
        self._print_end_of_row()
        self._print_horizontal_line()
        
    def _print_cell(self, message, colour='black', width=0, end=' | '):
        print('{}{:>{}}{}'.format(self.BCOLOUR[colour], message,
                                  width, self.BCOLOUR['black']), end=end)
        
    def _print_horizontal_line(self):
        self._print_cell('-'*self.char_per_line, 'black', self.char_per_line, '\n')
        
    def _print_start_of_row(self):
        stdout.write('| ')
        
    def _print_end_of_row(self):
        stdout.write('\n')
        stdout.flush()
        
        