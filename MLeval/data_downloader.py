from __future__ import print_function
from __future__ import division
import requests
import zipfile
import numpy as np
from sys import stdout
from os import makedirs
from os.path import dirname
from os.path import exists


class DataDownloader(object):

    BCOLOUR = {
        'purple': '\033[95m',
        'yellow': '\033[93m',
        'blue': '\033[94m',
        'green': '\033[92m',
        'red': '\033[91m',
        'cyan': '\033[36m',
        'black': '\033[0m'
    }
    CHUNK_SIZE = 32768
    DATA_URL = "https://gitlab.com/maxingaussian/MLeval-Data/raw/master/"

    def __init__(self, data_cache_dir='./data_cache/'):
        self.save_dir = dirname(data_cache_dir)
        if(not exists(self.save_dir)):
            makedirs(self.save_dir)
    
    def read_data(self, file_name):
        file_path = self.save_dir+'/'+file_name
        if(not exists(file_path)):
            print(self.BCOLOUR['red']+file_path, 'not found!')
            print(self.BCOLOUR['blue']+'downloading', file_name, 'into', self.save_dir, '......')
            response = requests.get(self.DATA_URL+file_name, stream=True)
            self._save_response_content(response, file_path)
            print(self.BCOLOUR['black']+'\ndone.')
        print(self.BCOLOUR['green']+'reading data in', file_name, '......')
        stdout.flush()
        zf = zipfile.ZipFile(file_path, 'r')
        parse = lambda s: s[2:-1].replace('\\t', '\t').replace('\\n', '\n')
        lines = list(filter(None, parse(str(zf.read('data.txt'))).split('\n')))
        data = list(map(lambda l: list(map(float, list(filter(None, l.split())))), lines))
        print(self.BCOLOUR['black']+'done.')
        return np.array(data)
    
    def _save_response_content(self, response, file_path):
        with open(file_path, "wb") as f:
            total_size = response.headers.get('content-length')
            if total_size is None: # no content length header
                f.write(response.content)
            else:
                downloaded = 0
                total_size = int(total_size)
            for chunk in response.iter_content(self.CHUNK_SIZE):
                if(chunk):
                    downloaded += len(chunk)
                    f.write(chunk)
                    done = int(78*downloaded/total_size)
                    stdout.write("\r[%s%s]"%('='*done, ' '*(78-done)))
                    stdout.flush()
    